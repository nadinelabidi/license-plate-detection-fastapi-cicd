import io
import os
import datetime
import base64
from io import BytesIO
from PIL import Image
from fastapi import FastAPI, Request, UploadFile
from fastapi.testclient import TestClient

import cv2
import torch
import numpy as np

from fastapi import FastAPI, File
from fastapi.responses import JSONResponse
import json
from setuptools import __version__ as setuptools_version
from packaging.version import parse as version_parse


import cv2
import torch
import logging


import datetime
import pytest
import warnings
warnings.filterwarnings("ignore", message=".*U.*package is deprecated.*", category=DeprecationWarning)


@pytest.fixture
def client():
    from app import app
    with TestClient(app) as client:
        yield client


def test_image_detect(client):
    print("Starting test_image_detect...")

    # Load test image
    with open('Cars14.png', 'rb') as f:
        image_data = f.read()

    # Send POST request to /object_detect endpoint
    response = client.post(
        '/license_plate_detection',
        files={'input_file': ('Cars14.png', image_data, 'image/jpeg')}
    )

    # Assert response status code
    assert response.status_code == 200
    

    # Assert response data
    data = response.json()['data']
    assert os.path.exists(data) == True

    # Clean up saved image
    os.remove(data)
    print("Test complete!")

