import io
import os
import datetime
import base64
from io import BytesIO
from PIL import Image
from fastapi import FastAPI, Request, UploadFile

import cv2
import torch
import numpy as np

from fastapi import FastAPI, File, HTTPException
from fastapi.responses import JSONResponse
import json


app = FastAPI()

DATETIME_FORMAT = "%Y-%m-%d_%H-%M-%S-%f"
UPLOAD_FOLDER = './static/'




@app.post("/license_plate_detection")
async def license_detection(request: Request,
                       input_file: UploadFile = File(...)):

    if request.method == "POST":
      json_result = []
      try:
        image_data=cv2.imdecode(np.fromstring(input_file.file.read(), np.uint8), cv2.IMREAD_COLOR)
        image_data=cv2.cvtColor(image_data, cv2.COLOR_BGR2RGB)
        print('image read')
        model = torch.hub.load('ultralytics/yolov5', 'custom', './best.pt')
        model.eval()
        results = model([image_data])
        print(results)
        labels, cordinates = results.xyxyn[0][:, -1], results.xyxyn[0][:, :-1]
        classes = ['license']
        n = len(labels)
        x_shape, y_shape = image_data.shape[1], image_data.shape[0]
        detected_plates = []
        for i in range(n):
          row = cordinates[i]
          label_idx = int(labels[i])
          text_d = classes[label_idx]
          if text_d == 'license':
            if row[4] >= 0.55:  # threshold value for detection
                x1, y1, x2, y2 = int(row[0]*x_shape), int(row[1]*y_shape), int(row[2]*x_shape), int(row[3]*y_shape)
                cv2.rectangle(image_data, (x1, y1), (x2, y2), (0, 255, 0), 2)  # bounding box
                cv2.rectangle(image_data, (x1, y1-20), (x2, y1), (0, 255, 0), -1)  # label background
                cv2.putText(image_data, text_d, (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 2)  # label text
                # Crop license plate and display
                plate_crop = Image.fromarray(image_data[y1:y2, x1:x2])
                filename = f"{datetime.datetime.now().strftime(DATETIME_FORMAT)}.jpeg"
                save_path = os.path.join(UPLOAD_FOLDER, filename)
                plate_crop.save(save_path)
                return JSONResponse({"data": save_path,
                                     "message": "object detected successfully",
                                     "errors": None,
                                     "detail": "no details"},
                                    status_code=200)
      except Exception as e:
        return JSONResponse({"data": None,
                                 "message": "error in processing image",
                                 "errors": str(e),
                                 "detail": "no details"},
                                status_code=500)


if __name__ == '__main__':
    import uvicorn
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', default='localhost')
    parser.add_argument('--port', default=8000)
    parser.add_argument('--precache-models', action='store_true', 
            help='Pre-cache all models in memory upon initialization, otherwise dynamically caches models')
    
    opt = parser.parse_args()

    app_str = 'app:app' #make the app string equal to whatever the name of this file is
    uvicorn.run(app_str, host=opt.host, port=opt.port, reload=True)
