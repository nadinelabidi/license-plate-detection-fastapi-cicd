# Use an official Python runtime as a parent image
FROM python:3.8
# Set the working directory to license-plate
WORKDIR /license-plate

# Copy the requirements file into the container at /app
COPY requirements.txt /license-plate
COPY . /license-plate


# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt
RUN apt-get update && apt-get install -y libgl1-mesa-glx

# Set an environment variable for the Flask app
ENV FAST_APP=app.py

# Expose port 8000 for the fastapi app
EXPOSE 8000

# Run the command to start the Flask app
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8000"]

