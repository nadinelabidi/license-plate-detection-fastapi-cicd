import requests

# set the url for the detection endpoint
url = "http://localhost:8000/predict"

# set the image file path
image_path = "/home/ubuntu/license-plate-detection-CI-CD/images/Cars12.png"

# read image as bytes
with open(image_path, "rb") as f:
    img_bytes = f.read()

# set the headers
headers = {
    "Content-Type": "multipart/form-data"
}

# create the payload data
data = {
    "img": ("image.jpg", img_bytes, "image/jpeg")
}

# make the POST request to the detection endpoint
response = requests.post(url, files=data, headers=headers)

# print the response
print(response.json())
